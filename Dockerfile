FROM node:10.15-alpine

RUN yarn global add @vue/cli @vue/cli-service-global

RUN mkdir -p /vuejs && mkdir -p /node_modules

COPY package.json ./vuejs
COPY .yarnrc ./vuejs

WORKDIR /vuejs
RUN cd /vuejs && yarn

EXPOSE 8080